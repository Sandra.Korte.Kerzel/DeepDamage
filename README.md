# DeepDamage

Deep learning to quantify and classify damage in dual phase steel. 

All code and images can be found in branch "base".


If you find this project useful please cite:

Kusche C, Reclik T, Freund M, Al-
Samman T, Kerzel U, Korte-Kerzel S (2019) Large area, high-resolution characterisation and classification of damage mechanisms in dualphase steel using deep learning. 

PLoS ONE 14(5):e0216493. 

https://doi.org/10.1371/journal.pone.0216493


   
